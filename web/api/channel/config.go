package channel

import (
	"net/url"
	"strings"
	"time"

	oven "dev.sum7.eu/genofire/oven-exporter/helper"

	"github.com/Kukoon/media-server/models"
)

type StreamIngress struct {
	OvenPolicySignedKey string        `config:"-" json:"-"`
	PolicyExpire        time.Duration `config:"-" json:"-"`
	WebRTC              string        `json:"webrtc"`
	RTMP                string        `json:"rtmp"`
	Policy              *oven.Policy  `config:"-" json:"policy,omitempty"`
}

// ForChannel fills StreamIngress by replacer with values for a defined channel
func (si *StreamIngress) ForChannel(policy *oven.Policy, r *strings.Replacer) StreamIngress {
	webrtc, _ := url.Parse(r.Replace(si.WebRTC))
	rtmp, _ := url.Parse(r.Replace(si.RTMP))

	var p *oven.Policy
	if si.OvenPolicySignedKey != "" {
		p = policy
		if si.WebRTC != "" {
			p.SignURL(webrtc, si.OvenPolicySignedKey, si.PolicyExpire)
		}
		if si.RTMP != "" {
			p.SignURL(rtmp, si.OvenPolicySignedKey, si.PolicyExpire)
		}
	}
	return StreamIngress{
		WebRTC: webrtc.String(),
		RTMP:   rtmp.String(),
		Policy: p,
	}
}

type StreamSource struct {
	Label     string `json:"label"`
	Type      string `json:"type"`
	File      string `json:"file"`
	FrameRate int    `json:"framerate,omitempty"`
}

// ForChannel fills StreamSource by replacer with values for a defined channel
func (ss *StreamSource) ForChannel(r *strings.Replacer) StreamSource {
	return StreamSource{
		Label:     ss.Label,
		Type:      ss.Type,
		File:      r.Replace(ss.File),
		FrameRate: ss.FrameRate,
	}
}

type ConfigStream struct {
	Streams []StreamSource `config:"stream_sources"`
	Ingress StreamIngress  `config:"ingress"`
}

// ForChannel fills ConfigStream by placeholder with values for a defined channel
func (cs *ConfigStream) ForChannel(expire *oven.Policy, obj models.Channel) ConfigStream {
	r := strings.NewReplacer("{ID}", obj.ID.String())
	channelStream := ConfigStream{
		Ingress: cs.Ingress.ForChannel(expire, r),
	}
	for _, s := range cs.Streams {
		channelStream.Streams = append(channelStream.Streams, s.ForChannel(r))
	}
	return channelStream
}
