package models

import (
	"strings"
)

type ConfigStream struct {
	PosterURL  string `config:"poster_url"`
	PreviewURL string `config:"preview_url"`
}

func (cs *ConfigStream) replacerInit(s *Stream) *strings.Replacer {
	r := strings.NewReplacer("{CHANNEL_ID}", s.ChannelID.String())
	return r
}

func (cs *ConfigStream) GetPosterURL(s *Stream) string {
	if s.Poster != "" {
		return s.Poster
	}
	r := cs.replacerInit(s)
	if cs != nil && r != nil {
		return r.Replace(cs.PosterURL)
	}
	return ""
}

func (cs *ConfigStream) GetPreviewURL(s *Stream) string {
	if s.Preview != "" {
		return s.Preview
	}
	r := cs.replacerInit(s)
	if cs != nil && r != nil {
		return r.Replace(cs.PreviewURL)
	}
	return ""
}
